import numpy as np
import pandas as pd 
import os
from os import listdir
from os.path import isfile, join

def read_csv(file_path):
	# file_path = 'news_dataset.csv'
	df = pd.read_csv(file_path)
	np_array = df.values
	print(np_array)


def read_file(file_path):
	# file_path = '../../10Topics/News/Train_Full/Kinh doanh/KD_NLD_ (3328).txt'
	with open(file_path, 'r', encoding='utf-16-le') as myfile:
		data = myfile.read()
	# print(data)
	return data

def read_folder(folder_path):
	# folder_path = '../../10Topics/News/Train_Full/Kinh doanh'
	files = [join(folder_path, f) for f in listdir(folder_path) if isfile(join(folder_path, f))]
	print(files)
	return files


def read_folder_data(folder_path):
	# folder_path = '../../10Topics/News/Train_Full/Kinh doanh'
	files = [join(folder_path, f) for f in listdir(folder_path) if isfile(join(folder_path, f))]
	# print(files)
	df = pd.DataFrame(columns=['text', 'label'])
	
	for idx, f in enumerate(files):
		data = read_file(f)
		label = os.path.basename(folder_path)
		df.loc[idx] = [data, label]

	print(df.shape)
	return df


def read_multiple_data(folder_path, label_list):
	folder_path = '/media/kyhoolee/learn/nlp-ai/week-1/Week 1/10Topics/News/Train_Full'
	label_list = [
		'Chinh tri Xa hoi',
		'The gioi',
		'Khoa hoc'
		]

	for label in label_list:
		read_folder_data(join(folder_path, label))


if __name__ == '__main__':
	#read_file('')
	#read_folder('')
	#read_folder_data('')
	read_multiple_data('', [])


